# ASR resources for Kaldi in PT\_BR :br:

Here you may find some scripts to perform live decoding with Kaldi in Brazilian
Portuguese.

:warning: Beware these scripts follow Kaldi's `nnet2` recipe based RM and WSJ
datasets, which appear to be out of date already in favour of `nnet3` recipes
for Librispeech or mini\_librispeech examples.

# Kaldi installation instructions

Clone Kaldi from their official GitHub repo:

```bash
$ git clone https://github.com/kaldi-asr/kaldi
```

Install Kaldi tools, including PortAudio. Here I am using version 8 of the
GCC suite because it is the only one in which the compilation was successful
under Arch Linux. Otherwise you may omit `CC`, `CXX` and `FC` flags, and just
follow `kaldi/tools/INSTALL` file guide.

```bash
$ cd kaldi/tools/
$ CC=gcc-8 CXX=g++-8 FC=gfortran-8 extras/check_dependencies.sh
$ CC=gcc-8 CXX=g++-8 FC=gfortran-8 make -j 6
$ CC=gcc-8 CXX=g++-8 FC=gfortran-8 extras/install_portaudio.sh
```

Finally, install Kaldi, including some extra tools for online decoding:

```bash
$ cd kaldi/src/
$ CC=gcc-8 CXX=g++-8 FC=gfortran-8 ./configure --shared
$ CC=gcc-8 CXX=g++-8 FC=gfortran-8 make depend -j 6
$ CC=gcc-8 CXX=g++-8 FC=gfortran-8 make -j 6
$ CC=gcc-8 CXX=g++-8 FC=gfortran-8 make ext -j 6
```

# Usage guide

The directory tree for new projects must follow the structure below:

```
           path/to/kaldi/egs/YOUR_PROJECT_NAME/
                                 ├─ path.sh
                                 ├─ cmd.sh
                                 ├─ online_decoding_gmm.sh
                                 └─ online_decoding_dnn.sh
```

Therefore, all you gotta do is to first clone this repo:

```bash
$ git clone https://gitlab.com/fb-asr/fb-asr-resources/kaldi-resources.git
```

And then copy the entire folder under the Kaldi's `egs` repo you just
installed:

```bash
$ cp kaldi-resources/ -rv path/to/kaldi/egs/
```

Scripts for online decoding can then be execute as follows:

```bash
$ cd kaldi/egs/kaldi-resources/
$ ./online_decoding_gmm.sh                  # GMM model on pre-recorded audio
$ ./online_decoding_gmm.sh --test-mode live # GMM model live with your own voice via mic input
$ ./online_decoding_dnn.sh                  # DNN model on pre-recorded audio
```

The default behaviour will be to decode audio files under the `audio/` dir.
Uppon success you may see some output like the following:

```text
$ bash online_decoding_gmm.sh

  SIMULATED ONLINE DECODING - pre-recorded audio is used

online-wav-gmm-decode-faster --verbose=1 --rt-min=0.8 --rt-max=0.85 --max-active=4000 --beam=12.0 --acoustic-scale=0.0769 scp:./online_decoding_gmm/input.scp am/tri3_8k_16/final.mdl am/tri3_8k_16/HCLG.fst am/tri3_8k_16/words.txt 1:2:3:4:5 ark,t:./online_decoding_gmm/trans.txt ark,t:./online_decoding_gmm/ali.txt am/tri3_8k_16/final.mat
File: falabrasil
esse é um teste do sistema de reconhecimento de cola desenvolvi pelo grupo cola brasil da universidade federal do pará

File: test1
esse indivíduo assis televisão demais

File: test2
hoje é onze de julho

File: test3
meu avô é muito velho
```

# Scripts and resources

* __online_decoding_gmm.sh__: This script is adapted from the Voxforge recipe for 
[online decoding](https://github.com/kaldi-asr/kaldi/tree/master/egs/voxforge/online_demo).
It is meant to demonstrate how an existing HMM-GMM can be used to decode new 
audio files.    
* __online_decoding_dnn.sh__: This script is meant to demonstrate how an 
existing DNN-HMM acoustic model trained with iVectors can be used to decode new 
audio files.    
* __audio/__: contains 3 audio files transcribed to work as examples.     
* __am/__: Acoustic model for Brazilian Portuguese using Kaldi tools.      
* __lm/lm.arpa__: A N-gram language model built with the [SRILM](http://www.speech.sri.com/projects/srilm/download.html) toolkit.

## Citation

If you use these resources, please cite us as one of the following:

> Batista, C., Dias, A.L., Sampaio Neto, N. (2018) Baseline Acoustic Models for
> Brazilian Portuguese Using Kaldi Tools. Proc. IberSPEECH 2018, 77-81, DOI:
> 10.21437/IberSPEECH.2018-17.

```bibtex
@inproceedings{Batista2018,
  author    = {Cassio Batista and Ana Larissa Dias and Nelson {Sampaio Neto}},
  title     = {{Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools}},
  year      = {2018},
  booktitle = {Proc. IberSPEECH 2018},
  pages     = {77--81},
  doi       = {10.21437/IberSPEECH.2018-17},
  url       = {http://dx.doi.org/10.21437/IberSPEECH.2018-17}
}
```    

[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2019)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Larissa Dias - larissa.engcomp@gmail.com    
Cassio Batista - https://cassota.gitlab.io/
